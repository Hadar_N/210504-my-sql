/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.mjs";
import {authMiddleware} from './user.auth.mjs'
import express from 'express';
import log from '@ajar/marker';
import { connection } from "../../db/mysql.connection.mjs";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json())

// CREATES A NEW USER
router.post("/", authMiddleware,raw(async (req, res) => {
    log.obj(req.body, "create a user, req.body:");
    console.log(req.body);
    const {first_name, last_name,email} = req.body;
    let query_res = await connection.query(`INSERT INTO users (first_name, last_name, email) 
                                                VALUES ('${first_name}', '${last_name}', '${email}')`);
    query_res = await connection.query(`SELECT * FROM users ORDER BY id DESC LIMIT 1`); 
    res.status(200).json(query_res[0][0]);
}));


// GET ALL USERS
router.get( "/",raw(async (req, res) => {
  let query_res = await (connection.query("SELECT * FROM users"));
  res.status(200).json(query_res[0]);
})
);

router.get('/paginate/:page?/:items?', raw( async(req, res)=> {

  const startIndex = req.params.items * (req.params.page-1)
  let query_res = await (connection.query(`SELECT * FROM users LIMIT ${startIndex}, ${req.params.items}`));
  res.status(200).json(query_res[0])

}))

// GETS A SINGLE USER
router.get("/:id",raw(async (req, res) => {
    let query_res = await (connection.query(`SELECT * FROM users WHERE id=${req.params.id}`));

    if(query_res[0].length) res.status(200).json(query_res[0][0]);
    else res.status(404).json({ status: "No user found." })
  })
);

// UPDATES A SINGLE USER
router.put("/:id",authMiddleware,raw(async (req, res) => {

  let patchquery = [];
  for(let item in req.body){
    patchquery.push(`${item} = '${req.body[item]}'`);
  };
  const query = `UPDATE users SET ${patchquery.join(" ,")} WHERE id=${req.params.id}`;

    let query_res = await (connection.query(query));
    let user = await (connection.query(`SELECT * FROM users WHERE id=${req.params.id}`));

    if(query_res[0].affectedRows) res.status(200).json(user[0][0]);
    else res.status(404).json({ status: "No user found." })

  })
);


// DELETES A USER
router.delete("/:id",raw(async (req, res) => {
    let user = await (connection.query(`SELECT * FROM users WHERE id=${req.params.id}`));
    let query_res = await (connection.query(`DELETE FROM users WHERE id=${req.params.id}`));

    if(query_res[0].affectedRows) res.status(200).json(user[0][0]);
    else res.status(404).json({ status: "No user found." })

  })
);

export default router;
